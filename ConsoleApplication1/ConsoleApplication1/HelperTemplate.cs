using System;
using System.IO;
using System.Linq;
using Ganss.Excel;

namespace ConsoleApplication1
{
    public static class HelperTemplate
    {
        private const string DatabasePath = @"F:\AmenitFolder\zdroj.xlsx";
        private const string TargetFolderPath = @"F:\AmenitFolder\Podpisy\";
        private const string PathToTemplate = @"F:\AmenitFolder\sablony\";
        
        
        public static void CheckDataAndCreateTemplate()
        {
            var peoples = new ExcelMapper(DatabasePath).Fetch<People>().ToList();
            
            foreach (var people in peoples)
            {
                var login = people.Login;
                var template = people.Sablona;
                
                Directory.CreateDirectory(TargetFolderPath + login);
                CopyTemplateDirectory(PathToTemplate + template, TargetFolderPath + login);
                ReplaceFilesInTemplate(people.Jmeno, people.Sablona, TargetFolderPath + login);
            }
        }

        private static void CopyTemplateDirectory(string sourcePath, string destinationPath)
        {
            foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", 
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", 
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(sourcePath, destinationPath), true);
        }

        private static void ReplaceFilesInTemplate(string name, string template, string path)
        {
            string[] array = {".htm",".txt", ".rtf"};

            foreach (var item in array)
            {
                ReplaceTextFiles(path + $@"\{template}{item}", name);    
            }
        }
        
        private static void ReplaceTextFiles(string source, string replaceName)
        {
            var file = new StreamReader(source);
            var content = file.ReadToEnd();
            
            if (content.Contains("%neco%"))
            {
                Console.WriteLine($"Chyba: v {source} je obsažen text %neco%");
            }
            file.Close();
            
            var writer = new StreamWriter(source);
            content = content.Replace("jmeno", $"{replaceName}");
            writer.Write(content);
            writer.Close();
        }
    }
}