﻿using System;

namespace ConsoleApplication1
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Editing template");
            
            HelperTemplate.CheckDataAndCreateTemplate();
            
            Console.WriteLine("Editing template is finished");
        }
    }
}